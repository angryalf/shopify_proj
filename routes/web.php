<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'BookingController@index')->middleware(['auth.shop'])->name('home');
Route::post('/booking/save', 'BookingController@create')->middleware(['auth.shop'])->name('booking.create');
Route::get('/booking/search', 'BookingController@search')->middleware(['auth.shop'])->name('booking.search');

//Auth::routes();

Route::get('/home', 'BookingController@index')->name('home');

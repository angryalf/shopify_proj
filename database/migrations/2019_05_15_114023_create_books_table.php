<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('workshop_id')->index();
            $table->bigInteger('group_id')->index();
            $table->bigInteger('customer_id')->index();
            $table->string('name')->nullable();
            $table->string('email' )->nullable();
            $table->string('phone', 18)->nullable();
            $table->boolean('guest')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}

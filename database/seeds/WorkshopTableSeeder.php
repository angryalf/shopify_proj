<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Workshop;

class WorkshopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [ 'date_start' => Carbon::parse('2019-06-01 09:00:00'), 'date_ends' => Carbon::parse('2019-06-01 12:00:00 '), 'maximum_guests' => 5 ],
            [ 'date_start' => Carbon::parse('2019-06-01 12:00:00'), 'date_ends' => Carbon::parse('2019-06-01 15:00:00 '), 'maximum_guests' => 5 ],
            [ 'date_start' => Carbon::parse('2019-06-01 15:00:00'), 'date_ends' => Carbon::parse('2019-06-01 18:00:00 '), 'maximum_guests' => 7 ],

            [ 'date_start' => Carbon::parse('2019-06-02 09:00:00'), 'date_ends' => Carbon::parse('2019-06-02 12:00:00 '), 'maximum_guests' => 5 ],
            [ 'date_start' => Carbon::parse('2019-06-02 12:00:00'), 'date_ends' => Carbon::parse('2019-06-02 15:00:00 '), 'maximum_guests' => 5 ],
            [ 'date_start' => Carbon::parse('2019-06-02 15:00:00'), 'date_ends' => Carbon::parse('2019-06-02 18:00:00 '), 'maximum_guests' => 12 ],

            [ 'date_start' => Carbon::parse('2019-06-03 09:00:00'), 'date_ends' => Carbon::parse('2019-06-03 12:00:00 '), 'maximum_guests' => 5 ],
            [ 'date_start' => Carbon::parse('2019-06-03 12:00:00'), 'date_ends' => Carbon::parse('2019-06-03 15:00:00 '), 'maximum_guests' => 7 ],
            [ 'date_start' => Carbon::parse('2019-06-03 15:00:00'), 'date_ends' => Carbon::parse('2019-06-03 18:00:00 '), 'maximum_guests' => 5 ],
        ];

        foreach ($data as $item){
            $workshop = Workshop::create($item);
        }

    }
}

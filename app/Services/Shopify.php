<?php

namespace App\Services;

use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use Request;

use OhMyBrew\ShopifyApp\Facades\ShopifyApp;


class Shopify
{



    public function __construct()
    {
    }


    public function getCustomers($filter=null)
    {
        $request = ShopifyApp::shop()->api()->rest('GET', '/admin/api/2019-04/customers.json');

        return collect($request->body->customers);
    }



}
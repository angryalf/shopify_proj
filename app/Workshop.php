<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Workshop extends Model
{
    public $timestamps = true;
    protected  $appends = ['name'];

    public function books()
    {
        return $this->hasMany(Book::class, 'workshop_id', 'id');
    }

    public function getNameAttribute()
    {
        return Carbon::parse($this->date_start)->format("M j gA") . " - " . Carbon::parse($this->date_ends)->format("gA") ;
    }

}

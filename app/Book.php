<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public $timestamps = true;

    protected $guarded = [];

    public function workshops()
    {
        return $this->belongsTo(Workshop::class, 'workshop_id', 'id');
    }

}

<?php

namespace App\Http\Controllers;

use App\Workshop;
use Illuminate\Http\Request;
use App\Services\Shopify;
use Illuminate\Support\Facades\Log;

use App\Book;


class BookingController extends Controller
{


    private $shopify;

    public function __construct(Shopify $shopify)
    {
        $this->shopify = $shopify;
    }


    public function index()
    {


        return view('booking.index');
    }

    public function search(Request $request)
    {
        $query = $request->get('query');
        $shopifyUsers = $this->shopify->getCustomers();
        $users = collect();

        $filtered = $shopifyUsers->filter(function ($value, $key) use ($query) {
            // filter users to match search pattern
            $a1 = ( strpos( strtolower($value->first_name), strtolower($query) ) === false ) ? false : true;
            $a2 = ( strpos( strtolower($value->last_name), strtolower($query) ) === false ) ? false : true;
            return ($a1 || $a2) ? true : false;
        });

        if ($filtered) {
            foreach ($filtered as $item){
                $users->push( $item );
            }
        }



        return response()->json( $users );
    }

    public function create(Request $request)
    {


        $users = collect($request->get('users'));
        $group_id = $users->where('guest', null )->first()['customer_id'];

        $workshop = Workshop::find(  $users->where('guest', null )->first()['workshop_id'] )->load('books');
        $shopifyUsers = $this->shopify->getCustomers();

        if ( $workshop->books->count() || count($users) > $workshop->maximum_quests ){
            if ($workshop->books->count() == $workshop->maximum_guests ) return response()->json([
                'message' => 'Sorry, there are no more available spots for this workshop'], 401);

            if ( $workshop->maximum_guests < ($workshop->books->count() + count($users)) || count($users) > $workshop->maximum_guests ) return response()->json([
                'message' => 'Sorry, there are only '.  ($workshop->maximum_guests - $workshop->books->count() ) .' available spots for this workshop'], 401);

            if ( $workshop->books()->where('group_id', $group_id)->first() &&  $workshop->books()->where('workshop_id', $workshop->id)->first() ) return response()->json([
                'message' => 'Sorry, the group is already in this workshop'], 401);

        }



        foreach ($users as $user){

            $book = new Book();
            $book->fill( array_merge($user,[
                'workshop_id'   => $workshop->id,
                'group_id'      => $group_id,
            ]) );

            $workshop->books()->save( $book );

        }




        return response()->json('ok');


    }





}

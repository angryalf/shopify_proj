@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">@lang('Please, fill out the form')</div>

                    <div class="card-body" id="bookingMembers">

                        <booking-component
                                           v-bind:workshop='@json( \App\Workshop::all()->toArray() )'
                        ></booking-component>


                    </div>

                    <div class="card-footer">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div style="display: none;" class="_guestLineTemplate">
        <div class="row _guestLine" style="margin-top: 1rem; border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
            <div class="col-8">
                <div class="form-group row">
                    <label for="" class="col-4 col-form-label">@lang('Guest Name')</label>
                    <div class="col-8">
                        <input type="text" name="guest_name[]" class="form-control" id="" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-4 col-form-label">@lang('Email')</label>
                    <div class="col-8">
                        <input type="text" name="guest_email[]" class="form-control" id="" value="">
                    </div>
                </div>
            </div>
            <div class="col-4">
                <button type="button" class="close"  aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>

@endsection
